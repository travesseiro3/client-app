package br.com.builders.clientapp.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import br.com.builders.clientapp.dto.ClienteAtualizadoDto;
import br.com.builders.clientapp.dto.ClienteNomeAtualizadoDto;
import br.com.builders.clientapp.dto.InfoGeralClienteDto;
import br.com.builders.clientapp.dto.NovoClienteDto;
import br.com.builders.clientapp.dto.mapper.ClienteMapper;
import br.com.builders.clientapp.model.Cliente;
import br.com.builders.clientapp.repository.ClienteRepository;
import br.com.builders.clientapp.utils.MockedModels;

@ExtendWith(MockitoExtension.class)
public class ClienteServiceTest {

	@InjectMocks
	private ClienteServiceImpl service;

	@Mock
	private ClienteRepository repository;

	@Test
	public void deleteByIdTest() {
		long id = 5L;

		when(repository.existsById(id)).thenReturn(true);
		doNothing().when(repository).deleteById(id);

		service.deleteById(id);

		verify(repository).existsById(id);
		verify(repository).deleteById(id);
	}

	@Test
	public void deleteByIdClienteNaoExistenteTest() {
		long id = 5L;

		when(repository.existsById(id)).thenReturn(false);

		service.deleteById(id);

		verify(repository).existsById(id);
		verify(repository, times(0)).deleteById(id);
	}

	@Test
	public void findByIdTest() {
		Cliente cliente = MockedModels.getMockedCliente();
		Optional<Cliente> optionalCliente = Optional.of(cliente);
		long id = cliente.getId();

		ResponseEntity<Optional<InfoGeralClienteDto>> valorEsperado = new ResponseEntity<Optional<InfoGeralClienteDto>>(
				ClienteMapper.infoGeralFromOptionalCliente(optionalCliente),
				HttpStatus.OK
		);

		when(repository.findById(id)).thenReturn(optionalCliente);
		assertEquals(valorEsperado, service.findById(id));
		verify(repository).findById(id);
	}

	@Test
	public void findAllSimplesTest() {
		List<Cliente> clientes = MockedModels.getMockedClientes(3);
		Page<Cliente> paginaClientes = new PageImpl<Cliente>(clientes);
		Pageable paginacao = PageRequest.of(0, 20);

		ResponseEntity<Page<InfoGeralClienteDto>> valorEsperado = new ResponseEntity<Page<InfoGeralClienteDto>>(
				ClienteMapper.pageInfoGeralFromPageCliente(paginaClientes),
				HttpStatus.OK
		);

		when(repository.findAll(any(Pageable.class))).thenReturn(paginaClientes);
		assertEquals(valorEsperado, service.findAll(paginacao, Optional.empty(), Optional.empty()));
		verify(repository).findAll(paginacao);
	}

	@Test
	public void findAllComQueryStringTest() {
		Cliente clienteFiltrado = new Cliente();
		clienteFiltrado.setNome("teste");
		clienteFiltrado.setCpf("123");

		List<Cliente> clientesFiltrados = Arrays.asList(clienteFiltrado);
		Page<Cliente> paginaClientes = new PageImpl<Cliente>(clientesFiltrados);
		ExampleMatcher matcher = ExampleMatcher.matchingAll().withIgnoreCase();
		Pageable paginacao = PageRequest.of(0, 20);

		ResponseEntity<Page<InfoGeralClienteDto>> valorEsperado = new ResponseEntity<Page<InfoGeralClienteDto>>(
				ClienteMapper.pageInfoGeralFromPageCliente(paginaClientes),
				HttpStatus.OK
		);

		when(repository.findAll(Example.of(clienteFiltrado, matcher), paginacao)).thenReturn(paginaClientes);
		assertEquals(valorEsperado,
			service.findAll(
				paginacao, Optional.of(clienteFiltrado.getNome()), Optional.of(clienteFiltrado.getCpf())
			)
		);
		verify(repository).findAll(Example.of(clienteFiltrado, matcher), paginacao);
	}

	@Test
	public void saveTest() {
		NovoClienteDto novoCliente = MockedModels.getMockedNovoCliente();
		Cliente cliente = ClienteMapper.clienteFromNovoCliente(novoCliente);

		ResponseEntity<InfoGeralClienteDto> valorEsperado = new ResponseEntity<InfoGeralClienteDto>(
				ClienteMapper.infoGeralFromCliente(cliente),
				HttpStatus.CREATED
		);

		when(repository.save(cliente)).thenReturn(cliente);
		assertEquals(valorEsperado, service.save(novoCliente));
		verify(repository).save(cliente);
	}

	@Test
	public void updateTest() {
		ClienteAtualizadoDto clienteAtualizado = MockedModels.getMockedClienteAtualizado();
		long id = 8L;
		Cliente cliente = ClienteMapper.clienteFromClienteAtualizado(clienteAtualizado, id);

		ResponseEntity<InfoGeralClienteDto> valorEsperado = new ResponseEntity<InfoGeralClienteDto>(
				ClienteMapper.infoGeralFromCliente(cliente),
				HttpStatus.OK
		);

		when(repository.save(cliente)).thenReturn(cliente);
		assertEquals(valorEsperado, service.update(id, clienteAtualizado));
		verify(repository).save(cliente);
	}

	@Test
	public void partialUpdateTest() {
		Cliente cliente = MockedModels.getMockedCliente();
		long idCliente = cliente.getId();

		ClienteNomeAtualizadoDto clienteComNomeAtualizado = new ClienteNomeAtualizadoDto();
		clienteComNomeAtualizado.setNome(cliente.getNome());

		ResponseEntity<InfoGeralClienteDto> valorEsperado = new ResponseEntity<InfoGeralClienteDto>(
				ClienteMapper.infoGeralFromCliente(cliente),
				HttpStatus.OK
		);

		when(repository.findById(idCliente)).thenReturn(Optional.of(cliente));
		when(repository.save(cliente)).thenReturn(cliente);

		assertEquals(valorEsperado, service.partialUpdate(idCliente, clienteComNomeAtualizado));

		verify(repository).findById(idCliente);
		verify(repository).save(cliente);
	}


	@Test
	public void partialUpdateClienteNaoExistenteTest() {
		String nome = "teste";
		long idCliente = 39L;

		ClienteNomeAtualizadoDto clienteComNomeAtualizado = new ClienteNomeAtualizadoDto();
		clienteComNomeAtualizado.setNome(nome);

		Cliente cliente = new Cliente();
		cliente.setId(idCliente);
		cliente.setNome(nome);

		ResponseEntity<InfoGeralClienteDto> valorEsperado = new ResponseEntity<InfoGeralClienteDto>(
				ClienteMapper.infoGeralFromCliente(cliente),
				HttpStatus.NOT_FOUND
		);

		when(repository.findById(idCliente)).thenReturn(Optional.empty());

		assertEquals(valorEsperado, service.partialUpdate(idCliente, clienteComNomeAtualizado));

		verify(repository).findById(idCliente);
		verify(repository, times(0)).save(cliente);
	}
}