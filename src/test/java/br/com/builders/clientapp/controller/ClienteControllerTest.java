package br.com.builders.clientapp.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import br.com.builders.clientapp.dto.ClienteAtualizadoDto;
import br.com.builders.clientapp.dto.ClienteNomeAtualizadoDto;
import br.com.builders.clientapp.dto.InfoGeralClienteDto;
import br.com.builders.clientapp.dto.NovoClienteDto;
import br.com.builders.clientapp.dto.mapper.ClienteMapper;
import br.com.builders.clientapp.model.Cliente;
import br.com.builders.clientapp.service.ClienteService;
import br.com.builders.clientapp.utils.MockedModels;

@ExtendWith(MockitoExtension.class)
@WebAppConfiguration
public class ClienteControllerTest {

	@InjectMocks
	ClienteController controller;

	@Mock
	ClienteService service;

	private MockMvc mockMvc;
	private ObjectMapper objectMapper;

	private static final String CLIENTE_MAPPING_ENDPOINT = "/cliente";

	@BeforeEach
	public void setup() {
		mockMvc = MockMvcBuilders.standaloneSetup(controller)
				.setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
				.build();

		objectMapper = new ObjectMapper()
				.registerModule(new JavaTimeModule())
				.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	}

	@Test
	public void deleteByIdEndpointTest() throws Exception {
		doNothing().when(service).deleteById(any(Long.class));

		mockMvc.perform(delete(CLIENTE_MAPPING_ENDPOINT + "/" + 3))
			.andExpect(status().isNoContent());
	}

	@Test
	public void findByIdEndpointTest() throws Exception {
		Cliente cliente = MockedModels.getMockedCliente();
		Optional<InfoGeralClienteDto> optionalInfoGeralCliente = ClienteMapper.infoGeralFromOptionalCliente(Optional.of(cliente));

		ResponseEntity<Optional<InfoGeralClienteDto>> responseBody =
				new ResponseEntity<Optional<InfoGeralClienteDto>>(optionalInfoGeralCliente, HttpStatus.OK);

		when(service.findById(any(Long.class))).thenReturn(responseBody);

		MvcResult result = mockMvc.perform(get(CLIENTE_MAPPING_ENDPOINT + "/" + 29))
			.andExpect(status().isOk()).andReturn();

		assertEquals(
				objectMapper.writeValueAsString(optionalInfoGeralCliente.get()),
				result.getResponse().getContentAsString());
	}

	@Test
	public void findAllEndpointTest() throws Exception {
		List<Cliente> clientes = MockedModels.getMockedClientes(4);
		Page<InfoGeralClienteDto> pageInfoGeralCliente = ClienteMapper.pageInfoGeralFromPageCliente(
				new PageImpl<Cliente>(clientes));

		ResponseEntity<Page<InfoGeralClienteDto>> responseBody =
				new ResponseEntity<Page<InfoGeralClienteDto>>(pageInfoGeralCliente, HttpStatus.OK);

		when(service.findAll(any(Pageable.class), eq(Optional.of("teste")), eq(Optional.empty()))).thenReturn(responseBody);

		MvcResult result = mockMvc.perform(get(CLIENTE_MAPPING_ENDPOINT + "?nome=teste"))
				.andExpect(status().isOk()).andReturn();

		assertEquals(
				objectMapper.writeValueAsString(pageInfoGeralCliente),
				result.getResponse().getContentAsString());
	}

	@Test
	public void saveEndpointTest() throws Exception {
		NovoClienteDto novoCliente = MockedModels.getMockedNovoCliente();
		Cliente cliente = ClienteMapper.clienteFromNovoCliente(novoCliente);
		InfoGeralClienteDto infoGeralCliente = ClienteMapper.infoGeralFromCliente(cliente);

		ResponseEntity<InfoGeralClienteDto> responseBody =
				new ResponseEntity<InfoGeralClienteDto>(infoGeralCliente, HttpStatus.CREATED);

		when(service.save(any(NovoClienteDto.class))).thenReturn(responseBody);

		MvcResult result = mockMvc.perform(
				post(CLIENTE_MAPPING_ENDPOINT)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(novoCliente))
			)
			.andExpect(status().isCreated()).andReturn();

		assertEquals(
				objectMapper.writeValueAsString(infoGeralCliente),
				result.getResponse().getContentAsString());
	}

	@Test
	public void updateEndpointTest() throws Exception {
		long id = 3L;
		ClienteAtualizadoDto clienteAtualizado = MockedModels.getMockedClienteAtualizado();
		Cliente cliente = ClienteMapper.clienteFromClienteAtualizado(clienteAtualizado, id);
		InfoGeralClienteDto infoGeralCliente = ClienteMapper.infoGeralFromCliente(cliente);

		ResponseEntity<InfoGeralClienteDto> responseBody =
				new ResponseEntity<InfoGeralClienteDto>(infoGeralCliente, HttpStatus.OK);

		when(service.update(eq(id), any(ClienteAtualizadoDto.class))).thenReturn(responseBody);

		MvcResult result = mockMvc.perform(
				put(CLIENTE_MAPPING_ENDPOINT + "/" + id)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(clienteAtualizado))
			)
			.andExpect(status().isOk()).andReturn();

		assertEquals(
				objectMapper.writeValueAsString(infoGeralCliente),
				result.getResponse().getContentAsString());
	}

	@Test
	public void partialUpdateEndpointTest() throws Exception {
		long id = 79L;
		String novoNome = "um nome melhor";

		Cliente cliente = MockedModels.getMockedCliente();
		cliente.setNome(novoNome);

		ClienteNomeAtualizadoDto clienteNomeAtualizadoDto = new ClienteNomeAtualizadoDto();
		clienteNomeAtualizadoDto.setNome(cliente.getNome());

		InfoGeralClienteDto infoGeralCliente = ClienteMapper.infoGeralFromCliente(cliente);

		ResponseEntity<InfoGeralClienteDto> responseBody =
				new ResponseEntity<InfoGeralClienteDto>(infoGeralCliente, HttpStatus.OK);

		when(service.partialUpdate(eq(id), any(ClienteNomeAtualizadoDto.class))).thenReturn(responseBody);

		MvcResult result = mockMvc.perform(
				patch(CLIENTE_MAPPING_ENDPOINT + "/" + id)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(clienteNomeAtualizadoDto))
			)
			.andExpect(status().isOk()).andReturn();

		assertEquals(
				objectMapper.writeValueAsString(infoGeralCliente),
				result.getResponse().getContentAsString());
	}
}