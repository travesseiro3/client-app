package br.com.builders.clientapp.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import br.com.builders.clientapp.dto.ClienteAtualizadoDto;
import br.com.builders.clientapp.dto.NovoClienteDto;
import br.com.builders.clientapp.dto.mapper.ClienteMapper;
import br.com.builders.clientapp.model.Cliente;

public class MockedModels {

	private MockedModels() {}

	public static Cliente getMockedCliente() {
		Cliente cliente = new Cliente();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(StringUtils.FORMATO_DATA);

		cliente.setNome("cliente de teste");
		cliente.setCpf("001.002.003.04");
		cliente.setDataNascimento(LocalDate.parse("01/01/2000", formatter));
		cliente.setId(3L);

		return cliente;
	}

	public static List<Cliente> getMockedClientes(int quantidadeElementos) {
		List<Cliente> clientes = new ArrayList<>();
		Cliente cliente;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(StringUtils.FORMATO_DATA);

		for (int i = 0; i < quantidadeElementos; i++) {
			cliente = new Cliente();

			cliente.setNome("cliente de teste " + i);
			cliente.setCpf(String.valueOf(i));
			cliente.setDataNascimento(LocalDate.parse("01/01/2000", formatter));
			cliente.setId(Long.valueOf(i));

			clientes.add(cliente);
		}

		return clientes;
	}

	public static NovoClienteDto getMockedNovoCliente() {
		return ClienteMapper.novoClienteFromCliente(getMockedCliente());
	}

	public static ClienteAtualizadoDto getMockedClienteAtualizado() {
		return ClienteMapper.clienteAtualizadoFromCliente(getMockedCliente());
	}
}