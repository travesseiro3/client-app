package br.com.builders.clientapp.utils;

public class StringUtils {

	private StringUtils() {}

	public static final String MENSAGEM_ERRO_CLIENTE_SEM_NOME = "O nome do cliente não pode estar em branco.";
	public static final String MENSAGEM_ERRO_CLIENTE_SEM_CPF = "O cpf do cliente não pode ser nulo.";
	public static final String MENSAGEM_ERRO_CLIENTE_SEM_DATA_DE_NASCIMENTO = "A data de nascimento do cliente não pode ser nula.";
	public static final String FORMATO_DATA = "dd/MM/yyyy";
}