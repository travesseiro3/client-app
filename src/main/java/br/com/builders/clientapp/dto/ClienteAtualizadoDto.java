package br.com.builders.clientapp.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.builders.clientapp.utils.StringUtils;

public class ClienteAtualizadoDto {

	@NotBlank(message = StringUtils.MENSAGEM_ERRO_CLIENTE_SEM_NOME)
	private String nome;

	@NotNull(message = StringUtils.MENSAGEM_ERRO_CLIENTE_SEM_CPF)
	private String cpf;

	@JsonFormat(pattern = StringUtils.FORMATO_DATA)
	@NotNull(message = StringUtils.MENSAGEM_ERRO_CLIENTE_SEM_DATA_DE_NASCIMENTO)
	private LocalDate dataNascimento;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
}