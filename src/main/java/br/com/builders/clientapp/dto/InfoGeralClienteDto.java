package br.com.builders.clientapp.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.builders.clientapp.utils.StringUtils;

public class InfoGeralClienteDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String nome;

	private String cpf;

	@JsonFormat(pattern = StringUtils.FORMATO_DATA)
	private LocalDate dataNascimento;

	public InfoGeralClienteDto(Long id, String nome, String cpf, LocalDate dataNascimento) {
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.dataNascimento = dataNascimento;
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getCpf() {
		return cpf;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public Integer getIdade() {
		if(dataNascimento == null) {
			return null;
		}

		return Period.between(dataNascimento, LocalDate.now()).getYears();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((dataNascimento == null) ? 0 : dataNascimento.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		InfoGeralClienteDto other = (InfoGeralClienteDto) obj;
		if(cpf == null) {
			if(other.cpf != null)
				return false;
		}
		else if(!cpf.equals(other.cpf))
			return false;
		if(dataNascimento == null) {
			if(other.dataNascimento != null)
				return false;
		}
		else if(!dataNascimento.equals(other.dataNascimento))
			return false;
		if(id == null) {
			if(other.id != null)
				return false;
		}
		else if(!id.equals(other.id))
			return false;
		if(nome == null) {
			if(other.nome != null)
				return false;
		}
		else if(!nome.equals(other.nome))
			return false;
		return true;
	}
}