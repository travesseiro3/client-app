package br.com.builders.clientapp.dto.mapper;

import java.util.Optional;

import org.springframework.data.domain.Page;

import br.com.builders.clientapp.dto.ClienteAtualizadoDto;
import br.com.builders.clientapp.dto.InfoGeralClienteDto;
import br.com.builders.clientapp.dto.NovoClienteDto;
import br.com.builders.clientapp.model.Cliente;

public class ClienteMapper {

	private	ClienteMapper() {}

	public static Cliente clienteFromNovoCliente(NovoClienteDto novoCliente) {
		Cliente cliente = new Cliente();

		cliente.setNome(novoCliente.getNome());
		cliente.setCpf(novoCliente.getCpf());
		cliente.setDataNascimento(novoCliente.getDataNascimento());

		return cliente;
	}

	public static NovoClienteDto novoClienteFromCliente(Cliente cliente) {
		NovoClienteDto novoCliente = new NovoClienteDto();

		novoCliente.setNome(cliente.getNome());
		novoCliente.setCpf(cliente.getCpf());
		novoCliente.setDataNascimento(cliente.getDataNascimento());

		return novoCliente;
	}

	public static Cliente clienteFromClienteAtualizado(ClienteAtualizadoDto clienteAtualizado, long id) {
		Cliente cliente = new Cliente();

		cliente.setId(id);
		cliente.setNome(clienteAtualizado.getNome());
		cliente.setCpf(clienteAtualizado.getCpf());
		cliente.setDataNascimento(clienteAtualizado.getDataNascimento());

		return cliente;
	}

	public static ClienteAtualizadoDto clienteAtualizadoFromCliente(Cliente cliente) {
		ClienteAtualizadoDto clienteAtualizado = new ClienteAtualizadoDto();

		clienteAtualizado.setNome(cliente.getNome());
		clienteAtualizado.setCpf(cliente.getCpf());
		clienteAtualizado.setDataNascimento(cliente.getDataNascimento());

		return clienteAtualizado;
	}

	public static InfoGeralClienteDto infoGeralFromCliente(Cliente cliente) {
		return new InfoGeralClienteDto(
				cliente.getId(),
				cliente.getNome(),
				cliente.getCpf(),
				cliente.getDataNascimento()
		);
	}

	public static Optional<InfoGeralClienteDto> infoGeralFromOptionalCliente(Optional<Cliente> optionalCliente) {
		return optionalCliente.isEmpty() ?
				Optional.empty() :
				Optional.of(infoGeralFromCliente(optionalCliente.get()));
	}

	public static Page<InfoGeralClienteDto> pageInfoGeralFromPageCliente(Page<Cliente> pageCliente) {
		return pageCliente.map(cliente -> infoGeralFromCliente(cliente));
	}
}