package br.com.builders.clientapp.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import br.com.builders.clientapp.utils.StringUtils;

public class ClienteNomeAtualizadoDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotBlank(message = StringUtils.MENSAGEM_ERRO_CLIENTE_SEM_NOME)
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}