package br.com.builders.clientapp.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import br.com.builders.clientapp.dto.ClienteAtualizadoDto;
import br.com.builders.clientapp.dto.ClienteNomeAtualizadoDto;
import br.com.builders.clientapp.dto.InfoGeralClienteDto;
import br.com.builders.clientapp.dto.NovoClienteDto;

public interface ClienteService {

	ResponseEntity<InfoGeralClienteDto> save(NovoClienteDto novoCliente);

	void deleteById(long id);

	ResponseEntity<InfoGeralClienteDto> update(long id, ClienteAtualizadoDto clienteAtualizado);

	ResponseEntity<InfoGeralClienteDto> partialUpdate(long id, ClienteNomeAtualizadoDto clienteComNomeAtualizado);

	ResponseEntity<Optional<InfoGeralClienteDto>> findById(long id);

	ResponseEntity<Page<InfoGeralClienteDto>> findAll(Pageable pageable, Optional<String> nome, Optional<String> cpf);
}