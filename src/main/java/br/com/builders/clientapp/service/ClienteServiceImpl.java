package br.com.builders.clientapp.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.builders.clientapp.dto.ClienteAtualizadoDto;
import br.com.builders.clientapp.dto.ClienteNomeAtualizadoDto;
import br.com.builders.clientapp.dto.InfoGeralClienteDto;
import br.com.builders.clientapp.dto.NovoClienteDto;
import br.com.builders.clientapp.dto.mapper.ClienteMapper;
import br.com.builders.clientapp.model.Cliente;
import br.com.builders.clientapp.repository.ClienteRepository;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository repository;

	public ResponseEntity<InfoGeralClienteDto> save(NovoClienteDto novoCliente) {
		Cliente cliente = repository.save(ClienteMapper.clienteFromNovoCliente(novoCliente));

		return new ResponseEntity<InfoGeralClienteDto>(
				ClienteMapper.infoGeralFromCliente(cliente),
				HttpStatus.CREATED
		);
	}

	public ResponseEntity<InfoGeralClienteDto> update(long id, ClienteAtualizadoDto clienteAtualizado) {
		Cliente cliente = ClienteMapper.clienteFromClienteAtualizado(clienteAtualizado, id);

		return new ResponseEntity<InfoGeralClienteDto>(
				ClienteMapper.infoGeralFromCliente(repository.save(cliente)),
				HttpStatus.OK
		);
	}

	public ResponseEntity<InfoGeralClienteDto> partialUpdate(long id, ClienteNomeAtualizadoDto clienteComNomeAtualizado) {

		Optional<Cliente> optionalCliente = repository.findById(id);
		Cliente cliente;

		if(optionalCliente.isEmpty()) {
			cliente = new Cliente();
			cliente.setId(id);
			cliente.setNome(clienteComNomeAtualizado.getNome());

			return new ResponseEntity<InfoGeralClienteDto>(
					ClienteMapper.infoGeralFromCliente(cliente),
					HttpStatus.NOT_FOUND
			);
		}

		cliente = optionalCliente.get();
		cliente.setNome(clienteComNomeAtualizado.getNome());

		return new ResponseEntity<InfoGeralClienteDto>(
				ClienteMapper.infoGeralFromCliente(repository.save(cliente)),
				HttpStatus.OK
		);
	}

	public void deleteById(long id) {
		if(repository.existsById(id)) {
			repository.deleteById(id);
		}
	}

	public ResponseEntity<Optional<InfoGeralClienteDto>> findById(long id) {
		Optional<Cliente> optionalCliente = repository.findById(id);

		return new ResponseEntity<Optional<InfoGeralClienteDto>>(
				ClienteMapper.infoGeralFromOptionalCliente(optionalCliente),
				HttpStatus.OK
		);
	}

	public ResponseEntity<Page<InfoGeralClienteDto>> findAll(Pageable pageable, Optional<String> nome, Optional<String> cpf) {

		if(nome != null && cpf != null && nome.isEmpty() && cpf.isEmpty()) {
			return new ResponseEntity<Page<InfoGeralClienteDto>>(
					ClienteMapper.pageInfoGeralFromPageCliente(repository.findAll(pageable)),
					HttpStatus.OK
			);
		}
		else {
			ExampleMatcher matcher = ExampleMatcher.matchingAll().withIgnoreCase();
			Cliente cliente = new Cliente();

			if(nome != null && nome.isPresent()) {
				cliente.setNome(nome.get());
			}
			if(cpf != null && cpf.isPresent()) {
				cliente.setCpf(cpf.get());
			}

			return new ResponseEntity<Page<InfoGeralClienteDto>>(
					ClienteMapper.pageInfoGeralFromPageCliente(repository.findAll(Example.of(cliente, matcher), pageable)),
					HttpStatus.OK
			);
		}
	}
}