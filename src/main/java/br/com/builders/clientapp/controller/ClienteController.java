package br.com.builders.clientapp.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.builders.clientapp.dto.ClienteAtualizadoDto;
import br.com.builders.clientapp.dto.ClienteNomeAtualizadoDto;
import br.com.builders.clientapp.dto.InfoGeralClienteDto;
import br.com.builders.clientapp.dto.NovoClienteDto;
import br.com.builders.clientapp.service.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api
@RestController
@CrossOrigin
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	private ClienteService service;

	@GetMapping
	@ApiOperation("Rota para buscar os clientes na API. Seu nome e cpf podem ser usados como filtro.")
	public ResponseEntity<Page<InfoGeralClienteDto>> findAll(Pageable pageable, @RequestParam Optional<String> nome, @RequestParam Optional<String> cpf) {
		return service.findAll(pageable, nome, cpf);
	}

	@GetMapping("/{id}")
	@ApiOperation("Rota para buscar um cliente na API através do seu id.")
	public ResponseEntity<Optional<InfoGeralClienteDto>> findById(@PathVariable long id) {
		return service.findById(id);
	}

	@PostMapping
	@ApiOperation("Rota para persistir um novo cliente na API.")
	public ResponseEntity<InfoGeralClienteDto> save(@RequestBody @Valid NovoClienteDto novoCliente) {
		return service.save(novoCliente);
	}

	@PutMapping("/{id}")
	@ApiOperation("Rota para atualizar um cliente na API.")
	public ResponseEntity<InfoGeralClienteDto> update(@RequestBody @Valid ClienteAtualizadoDto clienteAtualizado, @PathVariable long id) {
		return service.update(id, clienteAtualizado);
	}

	@PatchMapping("/{id}")
	@ApiOperation("Rota para atualizar alguns atributos de um cliente na API.")
	public ResponseEntity<InfoGeralClienteDto> partialUpdate(@RequestBody @Valid ClienteNomeAtualizadoDto clienteComNomeAtualizado, @PathVariable long id) {
		return service.partialUpdate(id, clienteComNomeAtualizado);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation("Rota para deletar um cliente na API através do seu id.")
	public void deleteById(@PathVariable long id) {
		service.deleteById(id);
	}
}