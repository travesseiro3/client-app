#!/bin/bash
# Script para construir a imagem do postgres e depois rodar um container dessa imagem
# tornando sua porta 5432 acessível à nossa máquina através da porta 5555 da nossa máquina

sudo docker build -t postgres-image .
sudo docker run -d --name postgres-container -p 5555:5432 postgres-image
